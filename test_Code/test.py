import networkx as nx
from itertools import combinations
import time

start_time = time.time()


def k_cliques(graph):
    # 2-cliques
    cliques = [{i, j} for i, j in graph.edges() if i != j]
    k = 2

    while cliques:
        # result
        yield k, cliques

        # merge k-cliques into (k+1)-cliques
        cliques_1 = set()
        for u, v in combinations(cliques, 2):
            w = u ^ v
            if len(w) == 2 and graph.has_edge(*w):
                cliques_1.add(tuple(u | w))

        # remove duplicates
        cliques = list(map(set, cliques_1))
        k += 1


def print_cliques(graph, size_k):
    for k, cliques in k_cliques(graph):
        if k == size_k:
            print('%d-cliques = %d, %s.' % (k, len(cliques), cliques))


G = nx.read_edgelist("/Users/a/MINF20/complexity/test_Code/Storage_10Com_50Users.txt",delimiter=',')

# Number of users in a community

size_k = 50
print_cliques(G, size_k)

print("--- %s seconds ---" % (time.time() - start_time))

# for clq in nx.clique.find_cliques(G):
#     print(clq)