import user.Edge;
import user.Graph;
import java.util.*;
import java.io.*;

public class Main {
    public static void main(String[] args) throws IOException  {
	// 1. Generate a random social network, represented as a graph.
	// create random graph with 50k user, each user has 1k friends
//	Graph.genRandomNetwork(50000, 1000);

	// 2. Generate a network with N communities, each has M members
	// create network with 5000 community, each has 100 users (500k users total)
	Graph b = Graph.genNetwork(10, 50);
//	b.printGraph();
	
	
	//// Write file
	

    File file = new File("/Users/a/MINF20/complexity/test_Code/Storage_10Com_50Users.txt");
    if (!file.exists()) {
        file.createNewFile();
    }
    FileWriter fw = new FileWriter(file);
    BufferedWriter bw = new BufferedWriter(fw);
    
//    for (int i = 0; i < aList.size(); i++) {
//        bw.write(aList.get(i).toString()+','+ 1);
//        bw.newLine();
//    }
    
	for (List<Edge> vList : b.getEdge()) {
	    if (vList.size() > 0) {
		System.out.print("" + vList.get(0).from().id());
		for (Edge edge : vList) {
			
			 String s1 = String.valueOf(edge.from().id());
			 String s2 = String.valueOf(edge.to().id());
					 
			 bw.write( s1+ ',' + s2 );
		     bw.newLine();
		     
//		     System.out.print(String.format(" (%d, %d, %.0f)", edge.from().id(), edge.to().id(), edge.weight()));
		}
		System.out.println();
	    }
	}
    
    bw.flush();
    bw.close();

    
    }
}
